\documentclass{beamer}
\usepackage{tikz}

\usetheme{EastLansing}
\setlength{\parskip}{15pt}
\title{Plant Care Assistant}
\institute{Group number - 2}
\begin{document}
\begin{frame}[t]{Plant Care Assistant}
    \large\textbf{SHRI VISHNU ENGINEERING COLLEGE FOR WOMEN}
    
    \centering\textbf{PROJECT NAME : PLANT CARE ASSISTANT}
    \begin{table}[h]
    \centering
    \begin{tabular}{ccc}
        \toprule
        \textbf{Regd.No} & \textbf{Name} & \textbf{Job Profile} \\
        \midrule
        22B01A4262 & M. Sri Kavya & Developer \\
        22B01A42A1 & S. Jahnavi & Developer \\
        22B01A4269 & Md. Zeenath Arifa & Latex\\
        22B01A4296 & S. Siva Harshini & Latex \\
        22B01A42B4 & V. Samatha & Gitlab\\
        \bottomrule
    \end{tabular}
    \end{table}
\end{frame}

\begin{frame}{DFD DIAGRAM}
    
\begin{figure}
    \centering
\begin{tikzpicture}[node distance=1.3cm, auto]
    % Nodes
    \node[draw, rectangle] (ui) {User Interface};
    \node[draw, rectangle, below of=ui] (auth) {User Authentication};
    \node[draw, rectangle, below of=auth] (db) {SQLite Database};
    \node[draw, rectangle, below of=db] (plant) {Plant Management System};
    \node[draw, rectangle, below of=plant] (notify) {Notification System};

    % Arrows
    \draw[->] (ui) -- (auth);
    \draw[->] (auth) -- (db);
    \draw[->] (db) -- (plant);
    \draw[->] (plant) -- (notify);
    \draw[->] (notify) -- (plant); % Feedback loop
\end{tikzpicture}
\end{figure}
\end{frame}
\begin{frame}[t]{Output Streams(starting Page)}
    \centering
    \hspace{1.0cm} % Adjust horizontal position, e.g., move the image 1cm to the left
    \includegraphics[width=0.4\textwidth]{plantcare3.png}
    \label{plantcare3.png}
    \begin{enumerate}
    \item Starting Page
        \begin{itemize}
        \item {Click on \textbf{START}} 
        \item {Here you'll go to sign-up page}
        \end{itemize}
\end{enumerate}

\end{frame}

\begin{frame}[t]{Output Streams(sign up Page)}
    \centering
    \hspace{1.0cm} % Adjust horizontal position, e.g., move the image 1cm to the left
    \includegraphics[width=0.4\textwidth]{sign-up.jpg}
    \label{sign-up.jpg}
    \begin{enumerate}
    \item Sign up Page
        \begin{itemize}
        \item {Click on \textbf{SIGN UP}} 
        \item {Here you'll go to home page}
        \end{itemize}
\end{enumerate}
\end{frame}
\begin{frame}[t]{Output Streams(log in Page)}
    \centering
    \hspace{1.0cm} % Adjust horizontal position, e.g., move the image 1cm to the left
    \includegraphics[width=0.4\textwidth]{log_in.jpg}
    \label{log_in.jpg}
    \begin{enumerate}
    \item Sign up Page
        \begin{itemize}
        \item {Click on \textbf{LOG IN}} 
        \item {If you are already sign in then directly login to home page}
        \end{itemize}
\end{enumerate}
\end{frame}
\begin{frame}{Plant Care Assistant}
    \frametitle{Conclusion}
    This \textbf {Plant Care Assistant's} Main aim is to remind the User through notification at the time set by user.
    Due to our busy lives we may forget about to water plants.So this Plant Care Assistant helps us to take care of plants. 
\end{frame}
\begin{frame}{Bibliography}
    \begin{enumerate}
        \item Author, A. (Year). \textit{Title of the First Book}. PublisherX.
        \item Researcher, R. B. (Year). \textit{Second Book on Plant Care Technologies}. Academic Press.
        \item Scientist, S., \& Expert, E. F. (Year). \textit{Advancements in Plant Monitoring}. Springer.
        
    \end{enumerate}
\end{frame}


\end{document}
